FUNCTION get_customer_account_info (p_external_customer IN NUMBER)
      RETURN account_info_table
   IS
      CURSOR custs (i_external_customer NUMBER)
      IS
         SELECT customer_id, custcode, cslevel
           FROM customer_all@bscsdb_lnk
          WHERE paymntresp = 'X'
            AND customer_id_ext = TO_CHAR (i_external_customer);

      l_acc_info_tab   account_info_table := account_info_table ();
      l_acc_info       account_info       DEFAULT NULL;
      l_count          INTEGER            := 0;
   BEGIN
      FOR cust IN custs (p_external_customer)
      LOOP
         BEGIN
            IF cust.custcode LIKE '1.%' OR cust.cslevel <> '10'
            THEN
               SELECT COUNT (co.customer_id)
                 INTO l_count
                 FROM sysadm.contract_all@bscsdb_lnk co,
                      sysadm.contract_history@bscsdb_lnk ch
                WHERE ch.co_id = co.co_id
                  AND ch.ch_seqno =
                                  (SELECT   MAX (ch_seqno)
                                       FROM sysadm.contract_history@bscsdb_lnk
                                      WHERE co_id = ch.co_id
                                   GROUP BY co_id)
                  AND ch.ch_status IN ('a', 's')
                  AND co.customer_id = cust.customer_id;
            ELSE
               SELECT COUNT (cs.customer_id)
                 INTO l_count
                 FROM customer_all@bscsdb_lnk cs
                WHERE cs.customer_id = cust.customer_id
                  AND EXISTS (
                         SELECT --+ first_rows
                                1
                           FROM sysadm.contract_all@bscsdb_lnk co,
                                sysadm.contract_history@bscsdb_lnk ch,
                                (SELECT     customer_id
                                       FROM customer_all@bscsdb_lnk
                                 START WITH customer_id IN (cust.customer_id)
                                 CONNECT BY PRIOR customer_id =
                                                              customer_id_high) cs2
                          WHERE ch.co_id = co.co_id
                            AND ch.ch_seqno =
                                   (SELECT   MAX (ch_seqno)
                                        FROM sysadm.contract_history@bscsdb_lnk
                                       WHERE co_id = ch.co_id
                                    GROUP BY co_id)
                            AND ch.ch_status IN ('a', 's')
                            AND co.customer_id = cs2.customer_id);
            END IF;

            IF l_count > 0
            THEN
               BEGIN                  
                  SELECT account_info (cs.custcode,
                                       CASE bm.bm_id
                                          WHEN 4
                                             THEN 'LARGE ACCOUNT'
                                          ELSE bm.bm_des
                                       END,
                                       ui.firstname || ' ' || ui.lastname,
                                       cs.cscomptaxno ,
                                       ui.msisdn 
                                      )
                    INTO l_acc_info
                    FROM customer_all@bscsdb_lnk cs,
                         billing_account@bscsdb_lnk ba,
                         bill_medium@bscsdb_lnk bm,
                         customers c,
                         ebpp_subscription sub,
                         ebpp_user_info ui
                    WHERE cs.customer_id = ba.customer_id
                     AND ba.csbillmedium = bm.bm_id                     
                     AND c.custcode = cs.custcode
                     AND sub.customer_id = c.customer_id
                     AND ui.user_id = sub.user_id
                     AND cs.customer_id = cust.customer_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     SELECT account_info
                                                 (cust.custcode,
                                                  CASE bm.bm_id
                                                     WHEN 4
                                                        THEN 'LARGE ACCOUNT'
                                                     ELSE bm.bm_des
                                                  END,
                                                  NVL (ccline2, ccline3),
                                                  cc.cscomptaxno,
                                                  NULL
                                                 )
                                INTO l_acc_info
                                FROM ccontact_all@bscsdb_lnk cc,
                                     billing_account@bscsdb_lnk ba,
                                     bill_medium@bscsdb_lnk bm,
                                     sysadm.contract_all@bscsdb_lnk co,
                                     rateplan@bscsdb_lnk ra
                               WHERE cc.customer_id = ba.customer_id
                                 AND ba.csbillmedium = bm.bm_id
                                 AND cc.ccbill = 'X'
                                 AND co.customer_id = cc.customer_id
                                 AND co.tmcode = ra.tmcode
                                 --   AND extrnl_rateplan_id IS NULL
                                                        --to exclude the prepaid rateplans
                                 AND cc.customer_id = cust.customer_id
                                 --  AND product_type <> 'PREPAID'
                                 AND co.tmcode NOT IN
                                        (413, 414, 415, 416, 417, 418, 419,
                                         420, 421, 422, 423, 424, 425, 426,
                                         427, 428, 429, 430, 431, 432, 433,
                                         434, 435, 436, 437, 478, 479, 534)
                                         and rownum <2;
               END;

               l_acc_info_tab.EXTEND;
               l_acc_info_tab (l_acc_info_tab.COUNT) := l_acc_info;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;
      END LOOP;

      RETURN l_acc_info_tab;
   EXCEPTION
      WHEN OTHERS
      THEN
         raise_application_error
                                (-20010,
                                    'estatement.get_customer_account_info - '
                                 || 'CustomerIdExt='
                                 || p_external_customer
                                 || TO_CHAR (SQLCODE)
                                 || SQLERRM
                                );
   END;
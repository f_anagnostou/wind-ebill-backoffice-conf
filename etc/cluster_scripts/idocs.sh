#!/bin/bash

# Script for the system cluster to start / stop idocs.
# This is supposed to run on the active cluster node by adding
# somewhere similar to /etc/init.d  an entry like
# su - idocs -c "/idocs/app/idocs/idocs.sh start "
# -or-
# su - idocs -c "/idocs/app/idocs/idocs.sh stop "
# @author a.giotis@i-docs.com



LOG=/idocs/log/idocs.sh.log
NODE_1=idocs1
NODE_2=idocs2
VIRTUAL_IP=10.20.21.67


if [ $# -ne 1 ] ; then
  echo "Usage: idocs.sh start|stop"
  exit 1
fi

# Modify path to find the jps command
PATH=/idocs/app/idocs/idocs-wind/lib/jdk1.6.0_11/bin:$PATH
JAVA_HOME=/idocs/app/idocs/idocs-wind/lib/jdk1.6.0_11
export JAVA_HOME

################# LOG FUNCTION ####################
log()
{
  echo "`date +%y%m%d\ %H:%M:%S` $*" >> ${LOG}
}
################# LOG FUNCTION ####################


# Determine the command line to execute on the active and standby node based
# on the start or stop parameter
action=$1
if [[ x${action} == xstart ]]; then
ACTIVE_CMD="nohup /idocs/app/idocs/idocs-wind/jboss-activeNode/bin/run.sh -b $VIRTUAL_IP &"
STANDBY_CMD="nohup /idocs/app/idocs/idocs-wind/jboss-standbyNode/bin/run.sh &"
fi

if [[ x${action} == xstop ]]; then
ACTIVE_CMD="/idocs/app/idocs/stop.sh >> /idocs/log/activeNode/nohup.out &"
STANDBY_CMD="/idocs/app/idocs/stop.sh >> /idocs/log/standbyNode/nohup.out &"
fi

if [[ x${ACTIVE_CMD} == x ]]; then
  echo "Usage: idocs.sh start|stop"
  exit 1
fi



# Execute start or stop cmd on the current active node
localhostname=`hostname`

log "${action}ing i-docs on the active node ${localhostname} "
${ACTIVE_CMD} >> ${LOG} 2>&1 &

# Determine the hostname of the standby node
if [[ ${localhostname} == ${NODE_1} ]]; then
    STANDBY_NODE=${NODE_2}
else
    if [[ ${localhostname} == ${NODE_2} ]]; then
        STANDBY_NODE=${NODE_1}
    fi
fi


# Protect in case STANDBY_NODE is not found (maybe on purpose)
if [ ! -z "${STANDBY_NODE}" ]; then
   # Check if the standby node is alive (ping)
   isalive=`/usr/sbin/ping  ${STANDBY_NODE} | grep alive`
   if [[ x$isalive == x ]]; then
     log "Will not ${action} i-docs on the NOT-ALIVE standby node ${STANDBY_NODE}"
   else
     log "${action}ing i-docs on the standby node ${STANDBY_NODE} "
     rsh ${STANDBY_NODE} "${STANDBY_CMD} " >> ${LOG} 2>&1 &
   fi
else
  log "Standby node not found: This node is not ${NODE1} or ${NODE2}"
fi


pid=`ps -u idocs | grep java | awk '{print $1}'`
if [[ x$pid == x ]]; then
    echo "i-docs is not running on this node"
    exit 0
fi

echo "Killing PID $pid"
kill $pid
sleep 5

# Check if the process is still running
pid=`ps -u idocs | grep java | awk '{print $1}'`
if [[ x$pid == x ]]; then
    exit 0
fi

echo "Forcibly killing still active $pid"
kill -9 $pid


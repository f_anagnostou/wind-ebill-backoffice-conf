sql
	alter table
	ebpp_subscription
	add
	   (
	   SMSORDERID varchar2 (60 char),  
	   SMSSTATUS number (3,0),
	   SMSTIMESTAMP TIMESTAMP(6)
	   );
	   
	alter table
	   registration_req_sub
	add
	   (
	   SMSORDERID varchar2 (60 char),  
	   SMSSTATUS number (3,0),
	   SMSTIMESTAMP TIMESTAMP(6)
	   );   
	   
	alter table
	   invoices
	add
	   (
	   SMSORDERID varchar2 (60 char),  
	   SMSSTATUS number (3,0),
	   SMSTIMESTAMP TIMESTAMP(6)
	   );   
	   
	alter table REGISTRATION_REQ_SUB add ESTMT_DELIVERY_STATUS NUMBER(10,0) default -1;
	alter table REGISTRATION_REQ_SUB add ESTMT_EMAIL_PROCESSED NUMBER(10,0) default 0;
	alter table REGISTRATION_REQ_SUB add MESSAGE_ID VARCHAR2(80 CHAR);
	alter table REGISTRATION_REQ_SUB add DELIVERY_STATUS_TIMESTAMP TIMESTAMP(6);	 

--new role for eservice = 6	

insert into ebpp_role values ('28','6','min e-mail bill','1')
insert into aa_role values ('28','min e-mail bill')   
	   
--update capabilities from admin tool
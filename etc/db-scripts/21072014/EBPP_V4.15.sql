-- Table EBPP_USER_INFO
alter table
   EBPP_USER_INFO
add
   (  
   PRE_REGISTERED  NUMBER(1,0) default 0
   );
   
-- Table EBPP_SUBSCRIPTION   
alter table
   EBPP_SUBSCRIPTION
add
   (  
   EMAIL VARCHAR2(255 BYTE),
   MSISDN VARCHAR2(30 BYTE),
   LANGUAGE VARCHAR2(2 CHAR) default 'el'
   );   

-- Migrate values from EBPP_USER_INFO
UPDATE EBPP_SUBSCRIPTION S SET S.EMAIL = (SELECT U.EMAIL FROM EBPP_USER_INFO U WHERE U.USER_ID = S.USER_ID);
UPDATE EBPP_SUBSCRIPTION S SET S.MSISDN = (SELECT U.MSISDN FROM EBPP_USER_INFO U WHERE U.USER_ID = S.USER_ID);
   
-- Table Invoices
alter table
   INVOICES
add
   (
   HAS_BROADBAND NUMBER(1,0) default 0,  
   RETENTION_ELIGIBLE NUMBER(1,0) default 0,
   CALL_ANALYSIS_INCLUDED NUMBER(1,0) default 0,
   LANGUAGE VARCHAR2(2 CHAR) default 'el'
   );   
   
alter table  INVOICES
   RENAME COLUMN TIMESTAMP TO DELIVERY_STATUS_TIMESTAMP;   
   
-- Roles

INSERT INTO AA_ROLE (ID, NAME) VALUES (S_AA_ROLE.NEXTVAL, 'E-bill email');

INSERT INTO AA_CAPABILITY
    SELECT S_AA_CAPABILITY.NEXTVAL, SPECIFICATIONID, NEGATIVE, R.ID FROM AA_CAPABILITY C, AA_ROLE R
    WHERE C.ROLEID IN (SELECT ID FROM AA_ROLE WHERE NAME = 'E-bill plus')
    AND R.NAME = 'E-bill email'
    
INSERT INTO EBPP_ROLE
    SELECT ID, 4, NAME, 1 FROM AA_ROLE WHERE NAME = 'E-bill email'    
    
-- Trigger UPDATE_AA_ROLES
create or replace trigger UPDATE_AA_ROLES 
AFTER INSERT OR UPDATE ON CUSTOMERS 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW 
DECLARE
  TMP_ROLE_ID NUMBER;
BEGIN

    SELECT ROLE_ID INTO TMP_ROLE_ID FROM EBPP_ROLE WHERE EBPP_ROLE_ID = :new.ESERVICE;
    
    UPDATE aa_users_roles SET ROLEID = TMP_ROLE_ID WHERE USERID IN (SELECT USER_ID FROM ebpp_subscription  WHERE customer_id=:new.CUSTOMER_ID);
    
    EXCEPTION
    WHEN NO_DATA_FOUND THEN    
      NULL;
  
END; 


CREATE OR REPLACE 
FUNCTION "GET_GENERIC_VAR" (CUSTOMER_ID NUMERIC, USER_ID NUMERIC)
   RETURN VARCHAR2
IS
BEGIN
   RETURN    'false';
END GET_GENERIC_VAR;

create or replace PROCEDURE "CUST_UPDATE_REGISTRATION" 
(
  CUSTCODEPARAM IN VARCHAR2  
, ESERVICEPARAM IN NUMBER  
, ESERVICEENABLEDPARAM IN NUMBER
, EMAILPARAM IN VARCHAR2  
, MSISDNPARAM IN VARCHAR2  
, LANGUAGEPARAM IN VARCHAR2  
) IS
eserviceValue NUMBER;
eserviceEnabledValue NUMBER;
BEGIN
  BEGIN

--  DBMS_OUTPUT.PUT_LINE('SELECTING PROD_SUBSCRIPTIONS with userId ' || USERIDPARAM );
    SELECT ESERVICE, ESERVICE_ENABLED into eserviceValue,eserviceEnabledValue FROM customers c WHERE c.custcode = custCodeParam;
    eserviceEnabledValue := coalesce(eserviceEnabledParam, eserviceEnabledValue);
    eserviceValue := coalesce(eserviceParam, eserviceValue);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
            raise_application_error (-20906,'No Customer found for cust_code ' || custCodeParam); 
     WHEN TOO_MANY_ROWS THEN
        raise_application_error (-20092,'More than one customers for custCode ' || custCodeParam);
  END;
  
--  DBMS_OUTPUT.PE('Updating Customer with custCode ' || CUSTCODEPARAM || ' with eservice ' || eserviceValue || ' eserviceEnabled ' || eserviceEnabledValue);
  UPDATE customers SET eservice = eserviceValue, eservice_enabled = eserviceEnabledValue WHERE custcode = custCodeParam;
  
--  DBMS_OUTPUT.PUT_LINE('Updating REGISTRATION TABLE of custCode ' || CUSTCODEPARAM || ' with eservice ' || eserviceParam);
  UPDATE REGISTRATION SET eservice = eserviceParam WHERE CUST_CODE = CUSTCODEPARAM; 
  
--  DBMS_OUTPUT.PUT_LINE('Updating REGISTRATION TABLE of custCode ' || CUSTCODEPARAM || ' with email ' || EMAILPARAM || ' msisdn ' || MSISDNPARAM || ' language ' || LANGUAGEPARAM);
  UPDATE EBPP_SUBSCRIPTION SET EMAIL=EMAILPARAM, MSISDN=MSISDNPARAM, LANGUAGE=LANGUAGEPARAM WHERE DISPLAY = CUSTCODEPARAM;  

  COMMIT;
  EXCEPTION 
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END CUST_UPDATE_REGISTRATION;





   
   
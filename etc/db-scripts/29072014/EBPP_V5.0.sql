create or replace FUNCTION "ADD_EBPPUSER_INFO" (
   PUSERID      NUMBER,
   PGID         NUMBER,
   PFIRSTNAME   VARCHAR2,
   PLASTNAME    VARCHAR2,
   PEMAIL       VARCHAR2,
   PMSISDN      VARCHAR2,
   PROLEID      NUMBER,
   PUID         NUMBER,
   PREREGISTERED NUMBER
)
   RETURN NUMBER
IS
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

  
   INSERT INTO EBPP_USER_INFO
               (USER_ID, GID, FIRSTNAME, LASTNAME, EMAIL, MSISDN,
                ROLE_ID, PARENT_UID, PRE_REGISTERED
               )
        VALUES (PUSERID, PGID, PFIRSTNAME, PLASTNAME, PEMAIL, PMSISDN,
                PROLEID, PUID, PREREGISTERED
               );

   COMMIT;
   RETURN PUSERID;
 
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         RAISE_APPLICATION_ERROR (-20000, 'CAN NOT CREATE USER!!:' || SQLERRM);
      END;

END ADD_EBPPUSER_INFO;

-- USER_SUBSCRIBE_CSTID
create or replace 
FUNCTION          "USER_SUBSCRIBE_CUSTOMER" (USERID NUMBER, CSTID NUMBER, NODE NUMBER, EMAIL VARCHAR2, MSISDN VARCHAR2, LANGUAGE VARCHAR2)
   RETURN NUMBER
IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   TMPVAR              NUMBER;
   V_PARENT_UID        EBPP_USER_INFO.PARENT_UID%TYPE;
   V_GID               CUSTOMERS.GID%TYPE;
   V_CUSTTYPE          CUSTOMERS.CUSTTYPE%TYPE;
   V_CUSTOMER_ID       CUSTOMERS.CUSTOMER_ID%TYPE;
   V_CST_NAVTREE_ID    CUSTOMERS.NAVTREE_ID%TYPE;
   V_SUBSCRIPTION_ID   EBPP_SUBSCRIPTION.SUBSCRIPTION_ID%TYPE;
   V_DISPLAY           EBPP_SUBSCRIPTION.DISPLAY%TYPE;
   V_NAVTREE_ID        EBPP_SUBSCRIPTION.NAVTREE_ID%TYPE;
   V_NEW_NODE          EBPP_SUBSCRIPTION.NAVTREE_ID%TYPE;
   V_EMAIL			   EBPP_SUBSCRIPTION.EMAIL%TYPE;
   V_MSISDN			   EBPP_SUBSCRIPTION.MSISDN%TYPE;
   V_LANGUAGE		   EBPP_SUBSCRIPTION.LANGUAGE%TYPE;
BEGIN
   TMPVAR := 0;
   V_NEW_NODE := NODE;
   V_EMAIL := EMAIL;
   V_MSISDN := MSISDN;
   V_LANGUAGE := LANGUAGE;

   SELECT GID, PARENT_UID
     INTO V_GID, V_PARENT_UID
     FROM EBPP_USER_INFO
    WHERE USER_ID = USERID;

   SELECT CUSTOMER_ID, NAVTREE_ID, CUSTCODE, CUSTTYPE
     INTO V_CUSTOMER_ID, V_CST_NAVTREE_ID, V_DISPLAY, V_CUSTTYPE
     FROM CUSTOMERS
    WHERE CUSTOMER_ID = CSTID AND GID = V_GID;

   -- Only one user can be root and subscribed to a specific
   -- custcode. Other, will be deleted!
   IF V_PARENT_UID IS NULL
   THEN
      -- User is root, he can delete others subscriptions!
      DELETE FROM EBPP_SUBSCRIPTION
            WHERE SUBSCRIPTION_ID IN (
                     SELECT S.SUBSCRIPTION_ID
                       FROM EBPP_USER_INFO U, EBPP_SUBSCRIPTION S
                      WHERE S.CUSTOMER_ID = V_CUSTOMER_ID
                        AND U.GID = V_GID
                        AND U.USER_ID <> USERID
                        AND (U.PARENT_UID IS NULL OR U.PARENT_UID <> USERID)
                        AND S.USER_ID = U.USER_ID);

      COMMIT;
   END IF;

   IF V_NEW_NODE IS NOT NULL
   THEN
      SELECT NAVTREE_ID
        INTO V_NEW_NODE
        FROM NAVTREE
       WHERE NAVTREE_ID = NODE AND GID = V_GID AND CUSTOMER_ID = V_CUSTOMER_ID;
   END IF;

   BEGIN
      SELECT S.SUBSCRIPTION_ID, S.NAVTREE_ID
        INTO V_SUBSCRIPTION_ID, V_NAVTREE_ID
        FROM EBPP_SUBSCRIPTION S
       WHERE S.USER_ID = USERID AND S.CUSTOMER_ID = V_CUSTOMER_ID;

      IF V_NEW_NODE IS NOT NULL
      THEN
         UPDATE EBPP_SUBSCRIPTION
            SET NAVTREE_ID = V_NEW_NODE
          WHERE SUBSCRIPTION_ID = V_SUBSCRIPTION_ID;

         COMMIT;
      END IF;

      RETURN V_SUBSCRIPTION_ID;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN
            IF V_NEW_NODE IS NULL
            THEN
               V_NEW_NODE := V_CST_NAVTREE_ID;
            END IF;

            SELECT SUBSCRIPTION_ID_SEQ.NEXTVAL
              INTO V_SUBSCRIPTION_ID
              FROM DUAL;

            INSERT INTO EBPP_SUBSCRIPTION
                        (SUBSCRIPTION_ID, USER_ID, CUSTOMER_ID,
                         NAVTREE_ID, DISPLAY, CUSTTYPE, EMAIL, MSISDN, LANGUAGE
                        )
                 VALUES (V_SUBSCRIPTION_ID, USERID, V_CUSTOMER_ID,
                         V_NEW_NODE, V_DISPLAY, V_CUSTTYPE, V_EMAIL, V_MSISDN, V_LANGUAGE
                        );

            COMMIT;
            RETURN V_SUBSCRIPTION_ID;
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
               BEGIN
                  RAISE_APPLICATION_ERROR
                     (-20000,
                      'USER_SUBSCRIBE_CSTID: REGISTRATION FOR USER EXISTS FOR THIS CUSTCODE'
                     );
               END;
         END;
      WHEN OTHERS
      THEN
         RAISE_APPLICATION_ERROR
                          (-20000,
                           'USER_SUBSCRIBE_CSTID:  ERROR :-p Node was null??'
                          );
   END;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      BEGIN
         RAISE_APPLICATION_ERROR
            (-20000,
             'USER_SUBSCRIBE_CUSTCODE: CUSTOMER CODE, OR USER, OR NODE DOES NOT EXIST OR DOES NOT MATCH!'
            );
      END;
END USER_SUBSCRIBE_CUSTOMER;


create or replace 
FUNCTION          "UPDATE_ESERVICE" (userId NUMBER, custCode VARCHAR2, eserviceParam NUMBER, eserviceEnabledParam NUMBER) RETURN NUMBER 
IS
eserviceValue NUMBER;
eserviceEnabledValue NUMBER;

BEGIN
  BEGIN

  DBMS_OUTPUT.PUT_LINE('SELECTING Customer with custCode ' || custCode );
    SELECT ESERVICE, ESERVICE_ENABLED into eserviceValue,eserviceEnabledValue FROM customers c WHERE c.custcode = custCode;
    eserviceEnabledValue := coalesce(eserviceEnabledParam, eserviceEnabledValue);
    eserviceValue := coalesce(eserviceParam, eserviceValue);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
            raise_application_error (-20906,'No Customer found for cust_code ' || custCode); 
     WHEN TOO_MANY_ROWS THEN
        raise_application_error (-20092,'More than one customers for custCode ' || custCode);
  END;
    
  DBMS_OUTPUT.PUT_LINE('Updating Customer with custCode ' || custCode || ' with eservice ' || eserviceValue || ' eserviceEnabled ' || eserviceEnabledValue);
  UPDATE customers SET eservice = eserviceValue WHERE custcode = custCode;
    
  COMMIT;
  RETURN 0;
   EXCEPTION 
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END UPDATE_ESERVICE;
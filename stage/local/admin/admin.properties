#
# The profile of the database.
#
# Valid values are: oracle, mysql
#
admin.database.profile = oracle

#
# A Hibernate Dialect for the admin tool database
#
#admin.hibernate.dialect = org.hibernate.dialect.MySQLDialect
admin.hibernate.dialect = com.idocs.base.hibernate.dialect.Oracle10gDialect

# Set to true, only the *first* time you are using a datasource, to create the schema. 
admin.hibernate.schemaUpdate = false

#
# A Spring redirect view, where the user will be redirected
# after a successful authentication. 
#
admin.default.after.login.view = redirect:/

#
# The default page of the admi tool.
#
admin.default.admin.view = redirect:/admin/main.html

#
# The url to redirect the user on authentication failure.
#
admin.auth.failure.url = /admin/login.html?error=true

#
# The number of days after the last user logout before the user's
# account expires.
#
admin.expire.after.days.of.inactivity = 180

#
# The cron expression that triggers the procedure that checks
# for expired user accounts. 
#
# The default expression trigger the procedure every night
# at 3am.
#
# For more information on cron expressions see:
# http://opensymphony.com/quartz/api/org/quartz/CronExpression.html
# and http://opensymphony.com/quartz/api/org/quartz/CronTrigger.html
#
admin.expire.user.accounts.cron.trigger = 0 0 3 * * ?

#
# The number of old passwords we keep in the database in order
# to prevent the user from using the same password.
#
admin.number.of.old.passwords = 50

#
# The location of the configuration file for the method level
# cache. (This cache is used to cache ACL values)
#
admin.cache.config.location = WEB-INF/conf/admin/ehcache.xml

#
# The time period within normal users can login into the system.
#
# Users like administrators usually should be able to access the system
# 24/7 and should have the 'Login non working hours' capability.
#
# The format of the below expression is 'HH:mm - HH:mm'. 
#
admin.allowed.time.period = 8:00 - 20:00

#
# Set if the system should deny user logins during weekends. 
#
# Users like administrator usually should be able to access the system
# 24/7 and should have the 'Login non working hours' capability
#
# Legal values: true | false
#
admin.deny.logins.during.weekends = true

#
# The number of successive failed authentication attempts before the
# users account is locked. 
#
admin.max.failed.authentication.attempts = 30

#
# The number of allowed concurrent user sessions.
#
admin.max.concurrent.user.sessions = 100

#
# Deny logins if maximum cuncurrent sessions are exceeded.
#
# Legal values: true | false
#
admin.deny.if.maximum.sessions.exceeded = false

#
# The number of days before the user's password expires.
#
admin.days.before.password.expires = 60

#
# The format of the username.
#
# The format must be a Java compatible regular expression. See:
# http://java.sun.com/j2se/1.5.0/docs/api/java/util/regex/Pattern.html
#
admin.username.format = [a-zA-Z][a-zA-Z0-9]{5,}

#
# A comma separated list of usernames reserved for system use.
#
# The below list must contain the username 'system' in order for the
# auditing subsystem to work correctly.
# 
admin.reserved.usernames = system

#
# The minimum number of characters a password must contain.
#
admin.minimum.password.length = 6

#
# Set to true to reject passwords that contain the username of the
# paricular user.  
#
# Legal values: true | false
#
admin.password.cannot.contain.username = false

#
# Set to true to reject passwords that contain two or more identical
# succesive characters.  
#
# Legal values: true | false
#
admin.password.succesive.characters = false

#
# Set to true to reject passwords that do not contain any digits.
#
# Legal values: true | false
#
admin.password.must.contain.digits = false

#
# The salt to use when encrypting passwords.
#
# If you want to disable the salting of passwords
# erase the value (but not the key) of this property.
# For example:
# 	admin.password.salt.value =  
#
# For security reasons, it should be different for
# every customer.
# 
# If the salt changes any users previously created
# will not be able to login. Do not change this
# property on a running system unless you are willing to 
# reset the database (erasing data or reseting all passwords).  
# 
admin.password.salt.value = idocs-7848975932

#Set to true for migration scenarios where the legacy password
#contains no salt. This will allow the users' passwords to be
#progressively updated with the given admin.password.salt.value
admin.password.salt.migrate = false

#
# Enable the auto-login functionality. 
#
# Legal values: true | false
#
admin.enable.autologin.url = true

#
# Which file to use for populating the admin database.
#
# The file must reside in ${idocs.conf.dir}/admin
#
admin.populate.data.file = populate-data.xml

#
# Make the membership property mandatory for user edits.
#
admin.user.membership.mandatory = true

#
# Maximum number of recently modified users' list
#
admin.modified.user.list.maxSize = 50

#
# Maximum number of last logged in users' list
#
admin.lastlogin.user.list.maxSize = 50

#
# Set the default locale to use before the user has logged-in or if the authentication
# provider may not resolve it (for example NTLM/LDAP authentication).
#
admin.default.locale = en

#
# Set the locales available when creating or editing a user existing in our database.
#
admin.available.locales = en, el

########################################################
## password recovery                                                                                                                           ##
########################################################

#
# Enables password recovery throughout a secret question
#
admin.enable.password.recovery = true


#
# Enables user unlock on password recovery!
#
admin.enable.userunlock.for.password.recovery = true

#
# Set the procedure style that will be used in order user to be informed about 
# his new password. It should be either via email or a redirected screen. 
#   
# Valid values are: email, redirect, userprovided (todo)
#
reset.password.procedure.style=userprovided

#
# if password recovery has been enabled, we need the following settings:
#
#   this is used to provide a list of questions to the user he has to answer in order to proceed
#
#   secretQuestionsProvider := { 		
#	com.idocs.aa.util.FixedSecretQuestionsProvider |   
#	com.idocs.efg.estmt.aa.ESBSecretQuestionsProvider | 
#       <furhter implementations>
#   }
#
#   this is used to validate the answers provided against a set of questions
#
#   answersValidationProvider := {
#       com.idocs.aa.util.FixedAnswerToSecretQuestionsValidationProvider |
#	com.idocs.efg.estmt.aa.ESBAnswersToSecretQuestionsValidationProvider |
#       <furhter implementations>
#   }
#   
#   this is used to finally persist the new password for the given user
#
#   passwordPersistenceProvider := {
#       com.idocs.aa.util.DefaultPasswordPersistenceProvider |
#       com.idocs.efg.estmt.aa.ESBPasswordPersistenceWSProvider |
#       <further implementations>
#   }
#
# com.idocs.efg.estmt.aa.ESBSecretQuestionsProvider
# com.idocs.efg.estmt.aa.ESBAnswersToSecretQuestionsValidationProvider
# com.idocs.efg.estmt.aa.ESBPasswordPersistenceWSProvider
recover.password.secretQuestionsProvider =     com.idocs.aa.util.DefaultSecretQuestionsProvider
recover.password.answersValidationProvider =   com.idocs.aa.util.DefaultAnswerToSecretQuestionsValidationProvider
recover.password.passwordPersistenceProvider = com.idocs.aa.util.DefaultPasswordPersistenceProvider

# if a list of secret questions is provided, this defines how
# many of them are maximally shown to the user; the system makes a 
# random choice if there are more questions in the list. 
# If this is set to "0", all questions are shown to the user 
# If there are not enough questions on the list, as many questions
# as possible are shown.
recover.password.secretQuestions.quantity = 3

recover.password.secretQuestions.retries = 2

# if this is set to 1 in a list with three questions,
# the user has to provide at least 2 correct answers
recover.password.secretQuestions.maxfails = 2

#
recover.password.secretQuestions.caseSensitive = false

# Ticket service. Set to the profile property that holds the mobile no of the user
admin.ticket.user.profile.sms.recipeint.property=phoneNumber
# Ticket service. Set to the profile property that holds the email of the user
admin.ticket.user.profile.email.recipeint.property=email




admin.notification.mailserver=192.168.30.70
admin.notification.sender=a.papadakis@i-docs.com

# Plain Text password used for migration between eBill and eCare (SSO).
admin.clear.password = XUWlKUI0EKa7CulsiUo8Yuy4j99cKWgYV\/B43p0R\+HQ\=

#
# This url is used for user impersonation. The username and password entered will be submitted 
#  at this url.
#
frontoffice.auth.url=http://localhost:6060/ebpp/authenticate

#
# The authority that user should have in order to perform impersonation. It needs prefix CAP_ or ROLE_
#
impersonation.authority=CAP_ADMINISTRATION

username.auth.ignore.case=true
# Must be false for production.
tryAuthInEbpp=true
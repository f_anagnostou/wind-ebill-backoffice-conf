<?xml version="1.0" encoding="UTF-8"?>

<!-- $Id: jboss-minimal.xml 60298 2007-02-05 16:01:55Z dimitris@jboss.org $ -->

<!-- ===================================================================== -->
<!--  A minimal JBoss Server Configuration                                 -->
<!-- ===================================================================== -->

<server>

   <!-- Load all jars from the JBOSS_DIST/server/<config>/lib directory. This
     can be restricted to specific jars by specifying them in the archives
     attribute.
    -->
   <classpath codebase="${jboss.server.lib.url:lib}" archives="*"/>
   
   <!-- ==================================================================== -->
   <!-- Thread Pool                                                          -->
   <!-- ==================================================================== -->
   
   <!-- A Thread pool service -->
   <mbean code="org.jboss.util.threadpool.BasicThreadPool"
      name="jboss.system:service=ThreadPool">
      <attribute name="Name">JBoss System Threads</attribute>
      <attribute name="ThreadGroupName">System Threads</attribute>
      <!-- How long a thread will live without any tasks in MS -->
      <attribute name="KeepAliveTime">60000</attribute>
      <!-- The max number of threads in the pool -->
      <attribute name="MaximumPoolSize">10</attribute>
      <!-- The max number of tasks before the queue is full -->
      <attribute name="MaximumQueueSize">1000</attribute>
      <!-- The behavior of the pool when a task is added and the queue is full.
      abort - a RuntimeException is thrown
      run - the calling thread executes the task
      wait - the calling thread blocks until the queue has room
      discard - the task is silently discarded without being run
      discardOldest - check to see if a task is about to complete and enque
         the new task if possible, else run the task in the calling thread
      -->
      <attribute name="BlockingMode">run</attribute>
   </mbean>
   
   <!-- ==================================================================== -->
   <!-- Log4j Initialization                                                 -->
   <!-- ==================================================================== -->
   <mbean code="org.jboss.logging.Log4jService"
      name="jboss.system:type=Log4jService,service=Logging">
      <attribute name="ConfigurationURL">resource:log4j.xml</attribute>
   </mbean>

   <!-- ==================================================================== -->
   <!-- JNDI                                                                 -->
   <!-- ==================================================================== -->
   
   <!-- A simple mbean wrapper around the jndi Naming object. This
   only handles an in memory instance. The NamingService uses this
   as the JNDI store and exposes it remotely.
   -->
   <mbean code="org.jnp.server.NamingBeanImpl"
      name="jboss:service=NamingBeanImpl"
      xmbean-dd="resource:xmdesc/NamingBean-xmbean.xml">
   </mbean>
   
   <mbean code="org.jboss.naming.NamingService"
      name="jboss:service=Naming"
      xmbean-dd="resource:xmdesc/NamingService-xmbean.xml">
      <!-- The call by value mode. true if all lookups are unmarshalled using
         the caller's TCL, false if in VM lookups return the value by reference.
      -->
      <attribute name="CallByValue">false</attribute>
      <!-- The listening port for the bootstrap JNP service. Set this to -1
         to run the NamingService without the JNP invoker listening port.
      -->
      <attribute name="Port">-1</attribute>
      <!-- The bootstrap JNP server bind address. This also sets the default
         RMI service bind address. Empty == all addresses
      -->
      <attribute name="BindAddress">${jboss.bind.address}</attribute>
      <!-- The port of the RMI naming service, 0 == anonymous -->
      <attribute name="RmiPort">0</attribute>
      <!-- The RMI service bind address. Empty == all addresses
      -->
      <attribute name="RmiBindAddress">${jboss.bind.address}</attribute>
      <!-- The thread pool service used to control the bootstrap lookups -->
      <!--<depends optional-attribute-name="LookupPool"
         proxy-type="attribute">jboss.system:service=ThreadPool</depends>-->
      <!-- An example of using the unifed invoker as the transport.
         <depends optional-attribute-name="InvokerProxyFactory"
         proxy-type="attribute">jboss:service=proxyFactory,type=unified,target=Naming</depends>
      -->
      <depends optional-attribute-name="Naming"
         proxy-type="attribute">jboss:service=NamingBeanImpl</depends>
   </mbean>
   
   <!-- ==================================================================== -->
   <!-- Security                                                             -->
   <!-- ==================================================================== -->

   <mbean code="org.jboss.security.plugins.SecurityConfig"
      name="jboss.security:service=SecurityConfig">
      <attribute name="LoginConfig">jboss.security:service=XMLLoginConfig</attribute>
   </mbean>
   <mbean code="org.jboss.security.auth.login.XMLLoginConfig"
      name="jboss.security:service=XMLLoginConfig">
      <attribute name="ConfigResource">login-config.xml</attribute>
   </mbean>

   <!-- JAAS security manager and realm mapping -->
   <mbean code="org.jboss.security.plugins.JaasSecurityManagerService"
      name="jboss.security:service=JaasSecurityManager">
      <!-- A flag which indicates whether the SecurityAssociation server mode
      is set on service creation. This is true by default since the
      SecurityAssociation should be thread local for multi-threaded server
      operation.
      -->
      <attribute name="ServerMode">true</attribute>
      <attribute name="SecurityManagerClassName">org.jboss.security.plugins.JaasSecurityManager</attribute>
      <attribute name="DefaultUnauthenticatedPrincipal">anonymous</attribute>
      <!-- DefaultCacheTimeout: Specifies the default timed cache policy timeout
      in seconds.
      If you want to disable caching of security credentials, set this to 0 to
      force authentication to occur every time. This has no affect if the
      AuthenticationCacheJndiName has been changed from the default value.
      -->
      <attribute name="DefaultCacheTimeout">1800</attribute>
      <!-- DefaultCacheResolution: Specifies the default timed cache policy
      resolution in seconds. This controls the interval at which the cache
      current timestamp is updated and should be less than the DefaultCacheTimeout
      in order for the timeout to be meaningful. This has no affect if the
      AuthenticationCacheJndiName has been changed from the default value.
      -->
      <attribute name="DefaultCacheResolution">60</attribute>
      <!-- DeepCopySubjectMode: This set the copy mode of subjects done by the
      security managers to be deep copies that makes copies of the subject
      principals and credentials if they are cloneable. It should be set to
      true if subject include mutable content that can be corrupted when
      multiple threads have the same identity and cache flushes/logout clearing
      the subject in one thread results in subject references affecting other
      threads.
      -->
      <attribute name="DeepCopySubjectMode">false</attribute>
   </mbean>
	
	<!-- ==================================================================== -->
   <!-- Transactions                                                         -->
   <!-- ==================================================================== -->

   <!-- The configurable Xid factory.  For use with Oracle, set pad to true -->
   <mbean code="org.jboss.tm.XidFactory"
      name="jboss:service=XidFactory">
      <!--attribute name="Pad">true</attribute-->
   </mbean>

   <!-- JBoss Transactions JTA -->
   <mbean code="com.arjuna.ats.jbossatx.jta.TransactionManagerService"
      name="jboss:service=TransactionManager">
      <attribute name="TransactionTimeout">300</attribute>
      <attribute name="ObjectStoreDir">${jboss.server.data.dir}/tx-object-store</attribute> 
   </mbean>
   
   <!--
      | UserTransaction support.
    -->
   <mbean code="org.jboss.tm.usertx.server.ClientUserTransactionService"
      name="jboss:service=ClientUserTransaction"
      xmbean-dd="resource:xmdesc/ClientUserTransaction-xmbean.xml">
      <depends>
         <mbean code="org.jboss.invocation.jrmp.server.JRMPProxyFactory"
            name="jboss:service=proxyFactory,target=ClientUserTransactionFactory">
            <attribute name="InvokerName">jboss:service=invoker,type=jrmp</attribute>
            <attribute name="TargetName">jboss:service=ClientUserTransaction</attribute>
            <attribute name="JndiName">UserTransactionSessionFactory</attribute>
            <attribute name="ExportedInterface">org.jboss.tm.usertx.interfaces.UserTransactionSessionFactory</attribute>
            <attribute name="ClientInterceptors">
               <interceptors>
                  <interceptor>org.jboss.proxy.ClientMethodInterceptor</interceptor>
                  <interceptor>org.jboss.invocation.InvokerInterceptor</interceptor>
               </interceptors>
            </attribute>
            <depends>jboss:service=invoker,type=jrmp</depends>
         </mbean>
      </depends>
      <depends optional-attribute-name="TxProxyName">
         <mbean code="org.jboss.invocation.jrmp.server.JRMPProxyFactory"
            name="jboss:service=proxyFactory,target=ClientUserTransaction">
            <attribute name="InvokerName">jboss:service=invoker,type=jrmp</attribute>
            <attribute name="TargetName">jboss:service=ClientUserTransaction</attribute>
            <attribute name="JndiName"></attribute>
            <attribute name="ExportedInterface">org.jboss.tm.usertx.interfaces.UserTransactionSession</attribute>
            <attribute name="ClientInterceptors">
               <interceptors>
                  <interceptor>org.jboss.proxy.ClientMethodInterceptor</interceptor>
                  <interceptor>org.jboss.invocation.InvokerInterceptor</interceptor>
               </interceptors>
            </attribute>
            <depends>jboss:service=invoker,type=jrmp</depends>
         </mbean>
      </depends>
   </mbean>
   
   <!-- ==================================================================== -->
   <!-- Invokers to the JMX node                                             -->
   <!-- ==================================================================== -->

   <!-- RMI/JRMP invoker -->
   <mbean code="org.jboss.invocation.jrmp.server.JRMPInvoker"
      name="jboss:service=invoker,type=jrmp">
      <attribute name="RMIObjectPort">7770</attribute>
      <attribute name="ServerAddress">${jboss.bind.address}</attribute>
      <!--
      <attribute name="RMIClientSocketFactory">custom</attribute>
      <attribute name="RMIServerSocketFactory">custom</attribute>
      <attribute name="RMIServerSocketAddr">custom</attribute>
      <attribute name="SecurityDomain">ssl-domain-name</attribute>
      -->
      <depends>jboss:service=TransactionManager</depends>
   </mbean>
	
	<!-- JACC security manager and realm mapping -->
  <mbean code="org.jboss.security.jacc.SecurityService"
    name="jboss.security:service=JACCSecurityService" xmbean-dd="">
    <xmbean>
      <description>The JACC security Policy service</description>
      <operation>
        <description>The start lifecycle operation</description>
        <name>start</name>
      </operation>
      <operation>
        <description>The stop lifecycle operation</description>
        <name>stop</name>
      </operation>
    </xmbean>
  </mbean>
  
   <!-- ==================================================================== -->
   <!-- Deployment Scanning                                                  -->
   <!-- ==================================================================== -->

   <!-- An mbean for hot deployment/undeployment of archives.
   -->
   <mbean code="org.jboss.deployment.scanner.URLDeploymentScanner"
      name="jboss.deployment:type=DeploymentScanner,flavor=URL">

      <depends optional-attribute-name="Deployer">jboss.system:service=MainDeployer</depends>

      <!-- The URLComparator can be used to specify a deployment ordering
           for deployments found in a scanned directory.  The class specified
           must be an implementation of java.util.Comparator, it must be able
           to compare two URL objects, and it must have a no-arg constructor.
           Two deployment comparators are shipped with JBoss:
             - org.jboss.deployment.DeploymentSorter 
               Sorts by file extension, as follows:
                 "sar", "service.xml", "rar", "jar", "war", "wsr", "ear", "zip", 
                 "*"
             - org.jboss.deployment.scanner.PrefixDeploymentSorter
               If the name portion of the url begins with 1 or more digits, those 
               digits are converted to an int (ignoring leading zeroes), and 
               files are deployed in that order.  Files that do not start with 
               any digits will be deployed last, and they will be sorted by
               extension as above with DeploymentSorter.
      -->
      <attribute name="URLComparator">org.jboss.deployment.DeploymentSorter</attribute>
      
      <!-- The Filter specifies a java.io.FileFilter for scanned
           directories.  Any file not accepted by this filter will not be
           deployed.  The org.jboss.deployment.scanner.DeploymentFilter 
           rejects the following patterns:
               "#*", "%*", ",*", ".*", "_$*", "*#", "*$", "*%", "*.BAK", 
               "*.old", "*.orig", "*.rej", "*.bak", "*,v", "*~", ".make.state", 
               ".nse_depinfo", "CVS", "CVS.admin", "RCS", "RCSLOG", "SCCS", 
               "TAGS", "core", "tags"
      -->
      <attribute name="Filter">org.jboss.deployment.scanner.DeploymentFilter</attribute>

      <!-- Disable period based scanning in the deploy directory -->
      <attribute name="ScanEnabled">false</attribute>

      <attribute name="ScanPeriod">120000</attribute>

	  <!-- The default for the following attribute is 60000 (i.e. 1 minute). The shutdown
	       hook, seems to always wait for that amount of time, waiting for the scanner
	       thread to finish. This resulted in a standard wait of 1 minute each time that
	       the JBoss process was shutdown, which is rather annoying. Setting this to 100ms
	       seems to be an acceptable alternative, in that the shutdown hook will give some
	       time to the scanner thread to figure out that there is a shutdown procedure in
	       progress, but will not sit idle for a whole minute.
	  -->
      <attribute name="StopTimeOut">100</attribute>

      <!-- URLs are comma seperated and unprefixed arguments are considered
         file URLs and resolve relative to server home(JBOSS_DIST/server/default)
         unless the given path is absolute. Any referenced directories cannot
         be unpackaged archives, use the parent directory of the unpacked
         archive.
       -->
      <attribute name="URLs">
       deploy/
      </attribute>
      
   </mbean>

</server>

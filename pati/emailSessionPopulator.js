function exec(form, userContext)
	{
	var request = Packages.com.idocs.web.pati.serverimpl.util.ServerUtil.getRequest();
	var session = request.getSession();
	var recipientId = session.getAttribute("emailRecipientId");
	var deliveryStatus = session.getAttribute("emailStatus");
	var dateFrom = session.getAttribute("emailDate_from");
	var dateTo = session.getAttribute("emailDate_to");
	
	Packages.java.lang.System.out.println("Value of recipient ID is "+recipientId);
	Packages.java.lang.System.out.println("Value of deliveryStatus is "+deliveryStatus);
	var autoSubmit = new Packages.com.idocs.web.pati.client.model.actions.impl.AutoFormSubmit();
	form.getLoadActions().add(autoSubmit);
	if (deliveryStatus!=null)
		{
		var status = form.getModelByName("emailStatus");
		status.getValues().add(new Packages.com.idocs.web.pati.client.model.values.GWTString(deliveryStatus));
		}
	
	if (recipientId!=null)
		{
		var recipient = form.getModelByName("emailRecipient");
		recipient.getValues().clear();
		recipient.getValues().add(new Packages.com.idocs.web.pati.client.model.values.GWTString(recipientId));
		}
	
	if (dateFrom!=null)
		{
		var date = form.getModelByName("emailDate");
		var formatter = new Packages.java.text.SimpleDateFormat("yyyyMMdd");
		var d = new Packages.com.idocs.web.pati.client.model.values.GWTDate(formatter.parse(dateFrom));
		date.getMinValues().set(0, d);
		}

	if (dateTo!=null)
		{
		var date = form.getModelByName("emailDate");
		var formatter = new Packages.java.text.SimpleDateFormat("yyyyMMdd");
		var d = new Packages.com.idocs.web.pati.client.model.values.GWTDate(formatter.parse(dateTo));
		date.getMaxValues().set(0, d);
		}
	
	}

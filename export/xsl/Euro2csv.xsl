<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="text" version="1.0" omit-xml-declaration="yes" encoding="UTF-8"/>
    <xsl:template match="/">        
    ContractNo;Bank;Type;CardNo;
        <xsl:value-of select="/Card/@contract_no"/>;<xsl:text>ΤΡΑΠΕΖΑ EFG EUROBANK ERGASIAS AE</xsl:text>;<xsl:value-of select="/Card/@type"/>;<xsl:call-template name="tokenize-card">
        	<xsl:with-param name="card_no" select="/Card/@card_no"/>
	</xsl:call-template>


       <!-- <xsl:for-each select="/Card/record/amount">
			<xsl:value-of select="./text()" />
			<xsl:text>&#10;</xsl:text>
	</xsl:for-each>-->
    </xsl:template>
    
    
    <xsl:template name="tokenize-card">
        <xsl:param name="card_no"/>
        <xsl:value-of select="substring($card_no, 1, 4)"/>
        <xsl:value-of select="'-'"/>
        <xsl:value-of select="substring($card_no, 6, 4)"/>
        <xsl:value-of select="'-'"/>
        <xsl:value-of select="substring($card_no, 11, 4)"/>
        <xsl:value-of select="'-'"/>
        <xsl:value-of select="substring($card_no, 16, 4)"/>
    </xsl:template>
</xsl:stylesheet>

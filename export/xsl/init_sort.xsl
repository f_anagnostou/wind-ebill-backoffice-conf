<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4" page-height="297mm" page-width="210mm">
					<xsl:variable name="layout" select="/Card/@layout"/>
					<xsl:variable name="type" select="/Card/@type"/>
					<xsl:variable name="version" select="/Card/@version"/>
					<fo:region-body region-name="PageBody" background-image="url('MC03_VC_10.png')" background-repeat="no-repeat"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			
			<fo:page-sequence master-reference="A4">
				<fo:flow flow-name="PageBody">
				
					<xsl:for-each select="/root/contents/text">
						<fo:block>
							<xsl:value-of select="."/>
						</fo:block>
					</xsl:for-each>
					
				</fo:flow>
			</fo:page-sequence>
			
		</fo:root>
	</xsl:template>

</xsl:stylesheet>

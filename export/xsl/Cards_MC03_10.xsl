<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:include href="Cards_Details_MC03_10.xsl"/>
	
	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4" page-height="297mm" page-width="210mm">
					<xsl:variable name="layout" select="/Card/@layout"/>
					<xsl:variable name="type" select="/Card/@type"/>
					<xsl:variable name="version" select="/Card/@version"/>
					<fo:region-body region-name="PageBody" background-image="url('MC03_VC_10.png')" background-repeat="no-repeat"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			
			<fo:page-sequence master-reference="A4">
				<fo:flow flow-name="PageBody">
					
					<!--Αριθμός Σελίδων-->
					<xsl:variable name="pagecount" select="count(/Card/pages)"/>
					
					<!--Για κάθε σελίδα-->
					<xsl:for-each select="/Card/pages">
						
						<xsl:call-template name="customers">
							<xsl:with-param name="customer" select="/Card/holder"/>
							<xsl:with-param name="root" select=".."/>
							
						</xsl:call-template>
						
						<xsl:call-template name="elta">
							<xsl:with-param name="sum" select="/Card/summary"/>
							<xsl:with-param name="customer" select="/Card/holder"/>
						</xsl:call-template>
						
						<xsl:call-template name="messages">
							<xsl:with-param name="mes" select="./message"/>
						</xsl:call-template>
						
						<fo:block break-after="page">
							<!--Για κάθε record στη σελίδα-->
							<xsl:for-each select="./record">
								<xsl:call-template name="details">
									<xsl:with-param name="rec" select="."/>
								</xsl:call-template>
							</xsl:for-each>
							
							<xsl:if test="count(/Card/pages)=position()">
								
								<xsl:call-template name="summary">
									<xsl:with-param name="sum" select="/Card/summary"/>
								</xsl:call-template>
								
								<xsl:call-template name="header">
									<xsl:with-param name="head" select="/Card/statement"/>
								</xsl:call-template>
								<fo:block id="last-page"/>
							</xsl:if>
						</fo:block>
						
					</xsl:for-each>
					
					<!--Για τη τελευταία σελίδα-->
						
					
						
				</fo:flow>
			</fo:page-sequence>
			
		</fo:root>
	</xsl:template>

</xsl:stylesheet>

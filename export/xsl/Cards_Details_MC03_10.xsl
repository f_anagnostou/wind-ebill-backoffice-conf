<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"  xmlns:fo="http://www.w3.org/1999/XSL/Format"
xmlns:rx="http://www.renderx.com/XSL/Extensions" >
	<xsl:decimal-format decimal-separator="," grouping-separator="." name="el_GR" />
	<xsl:preserve-space elements="ocrb"/>
	
	<xsl:template name="details">
		<xsl:param name="rec"/>
		
		<fo:table padding-top="0.5mm">
			<fo:table-column column-width="22mm"/>
			<fo:table-column column-width="29mm"/>
			<fo:table-column column-width="22mm"/>
			<fo:table-column column-width="103mm"/>
			<fo:table-column column-width="25mm"/>
					
			<fo:table-body>
				<fo:table-row>
				
					<fo:table-cell font="8pt Courier New" font-weight="bold" text-align="left" padding-left="11mm">
						<fo:block>
							<xsl:if test="$rec/trans_date!=''">
								<xsl:value-of select="$rec/trans_date"/>
							<!--xsl:call-template name="format-date1">
								<xsl:with-param name="date" select="$rec/trans_date" />
							</xsl:call-template-->
							</xsl:if>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font="8pt Courier New" font-weight="bold" text-align="left" padding-left="10mm" wrap-option="no-wrap">
						<fo:block>
							<xsl:value-of select="$rec/issue_no"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font="8pt Courier New" font-weight="bold" text-align="left" padding-left="10mm">
						<fo:block>
							<xsl:if test="$rec/issue_date!=''">
								<xsl:value-of select="$rec/issue_date"/>
								<!--xsl:call-template name="format-date1">
									<xsl:with-param name="date" select="$rec/issue_date" />
								</xsl:call-template-->
							</xsl:if>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font="8pt Courier New" font-weight="bold" text-align="left" padding-left="10mm">
						<fo:block>
							<xsl:value-of select="$rec/descr"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font="8pt Courier New " font-weight="bold" text-align="right" padding-left="10mm">
						<fo:block>
							<!--xsl:value-of select="$rec/amount"/-->
							<xsl:if test="$rec/amount!=''">
							<xsl:value-of select="format-number($rec/amount,'#.##0,00', 'el_GR')" />
							</xsl:if>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	    
	<xsl:template name="messages">
		
		<xsl:param name="mes"/>
		
		<fo:block-container absolute-position="fixed"  top="159mm" left="11mm" text-align="left">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="$mes/line1"/>
			</fo:block >
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="164mm" left="11mm" text-align="left">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="$mes/line2"/>
			</fo:block>
		</fo:block-container>			
		
		<fo:block-container absolute-position="fixed"  top="169mm" left="11mm" text-align="left">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="$mes/line3"/>
			</fo:block>
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="179mm" left="11mm" text-align="left">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="$mes/line4"/>
			</fo:block>
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="184mm" left="11mm" text-align="left">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="$mes/line5"/>
			</fo:block>
		</fo:block-container>		
		
		<fo:block-container absolute-position="fixed"  top="189mm" left="11mm" text-align="left">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="$mes/line6"/>
			</fo:block>
		</fo:block-container>	
		
	</xsl:template>
	
	<xsl:template name="header">
		<xsl:param name="head"/>
		
		<rx:pinpoint value="COUNTER" />
		
		<fo:block-container absolute-position="fixed"  top="31mm" right="12mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:if test="//min_payment!=''">
					<xsl:value-of select="format-number(//min_payment,'#.##0,00', 'el_GR')" />
				</xsl:if>
			</fo:block>
		</fo:block-container>
		
		<fo:block-container absolute-position="fixed"  top="46mm" right="16mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				
				<xsl:if test="//due_date!=''">
					<xsl:call-template name="format-date2">
						<xsl:with-param name="date" select="//due_date" />
					</xsl:call-template>
				</xsl:if>
				
			</fo:block>
		</fo:block-container>
		
		<fo:block-container absolute-position="fixed"  top="46mm" right="72mm" text-align="right">
			<fo:block font="10pt Courier New"> 
				
				<xsl:if test="//issue_date!='0'">
					<xsl:call-template name="format-date2">
						<xsl:with-param name="date" select="//issue_date" />
					</xsl:call-template>
				</xsl:if>
			</fo:block>
		</fo:block-container>
		
	</xsl:template>
    
	<xsl:template name="customers">
		
		<xsl:param name="customer"/>
		<xsl:param name="root"/>
		
		
		<fo:block font-family="Courier New" font-size="7pt" text-align="right" padding-top="16mm" padding-bottom="4mm" margin-right="10mm" font-weight="bold">
			<xsl:value-of select="$root/@stmt_no"/>
		</fo:block >
		
		
		<fo:table padding-top="9mm">
			<fo:table-column column-width="103mm"/>
			<fo:table-column column-width="58mm"/>
			<fo:table-column column-width="37mm"/>
			<fo:table-body>
				
				<fo:table-row>
					<fo:table-cell font="10pt Courier New "  text-align="left" padding-left="10mm" >
						<fo:block wrap-option="no-wrap">
							<xsl:value-of select="substring($customer/name,1,45)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font="10pt Courier New" font-weight="bold" text-align="left" padding-left="10mm">
						<fo:block>
							<xsl:value-of select="$root/@card_no"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font="10pt Courier New" font-weight="bold"  text-align="right" padding-left="10mm">
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell font="10pt Courier New "  text-align="left" padding-left="10mm" wrap-option="no-wrap">
						<fo:block>
							<xsl:value-of select="substring($customer/address,1,45)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  padding-left="10mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="10mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell font="10pt Courier New "  text-align="left" padding-left="10mm" >
						<fo:block>
							<xsl:value-of select="$customer/zip_code"/> - <xsl:value-of select="substring($customer/town,1,36)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  font="10pt Courier New " text-align="center" padding-left="10mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font="10pt Courier New" font-weight="bold" text-align="center" padding-left="10mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					
					<xsl:if test="not($customer/co-owner)">
						<fo:table-cell font="10pt Courier New "  text-align="left" padding-left="10mm" >
							<fo:block font="10pt Courier New " padding-left="10mm" color="white">.</fo:block>
						 </fo:table-cell>
					</xsl:if>
					
					<xsl:if test="$customer/co-owner!=''">
					<fo:table-cell font="10pt Courier New "  text-align="left" padding-left="10mm" >
						<fo:block>
							<xsl:value-of select="substring($customer/co-owner,1,45)"/>
						</fo:block>
					</fo:table-cell>
					</xsl:if>
					
					<fo:table-cell  font="10pt Courier New " text-align="center" padding-left="10mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font="10pt Courier New" font-weight="bold" text-align="center" padding-left="10mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell font="8pt Courier New "  text-align="left" padding-left="10mm" padding-top="4mm" padding-bottom="8mm">
						<fo:block>
							(<fo:page-number />/<fo:page-number-citation ref-id="last-page" />) 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  font="10pt Courier New " text-align="center" padding-left="10mm" padding-bottom="18mm">
						<fo:block>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font="10pt Courier New" font-weight="bold" text-align="center" padding-left="10mm"  padding-bottom="18mm" >
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				
			</fo:table-body>
		</fo:table>
		
	</xsl:template>
	
	<xsl:template name="elta">
		<xsl:param name="sum"/>
		<xsl:param name="customer"/>
		
		<fo:block-container absolute-position="fixed"  top="244mm" left="114mm" text-align="left">
			<fo:block font="10pt Courier New " wrap-option="no-wrap"> 
				<xsl:value-of select="substring($customer/name,1,30)"/>
			</fo:block>
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="249mm" left="114mm" text-align="left">
			<fo:block font="10pt Courier New " wrap-option="no-wrap"> 
				<xsl:value-of select="substring($customer/address,1,30)"/>
			</fo:block>
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="254mm" left="114mm" text-align="left">
			<fo:block font="10pt Courier New " wrap-option="no-wrap"> 
				<xsl:value-of select="$customer/zip_code"/>
				<fo:inline color="white">a</fo:inline>
				<xsl:value-of select="substring($customer/town,1,21)"/>
			</fo:block>
		</fo:block-container>
		
		<xsl:if test="not($customer/co-owner)">
			<fo:block-container absolute-position="fixed"  top="259mm" left="114mm" text-align="left">
				<fo:block font="10pt Courier New " wrap-option="no-wrap"/> 
			</fo:block-container>
		</xsl:if>
		
		<xsl:if test="$customer/co-owner!=''">
		<fo:block-container absolute-position="fixed"  top="259mm" left="114mm" text-align="left">
			<fo:block font="10pt Courier New " wrap-option="no-wrap"> 
				<xsl:value-of select="substring($customer/co-owner,1,30)"/>
			</fo:block>
		</fo:block-container>
		</xsl:if>
		
		<fo:block-container absolute-position="fixed"  top="285mm" left="40mm" text-align="left">
			<fo:block font="12pt OCR-B" white-space-collapse="false"> 
				<xsl:value-of select="$sum/ocrb"/>
			</fo:block>
		</fo:block-container>	
		
	</xsl:template>
	
	<xsl:template name="summary">
		<xsl:param name="sum"/>
		
		<fo:block-container absolute-position="fixed"  top="154mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				
				<xsl:if test="//prev_balance!=''">
					<xsl:value-of select="format-number(//prev_balance,'#.##0,00', 'el_GR')" />
				</xsl:if>
			</fo:block>
		</fo:block-container>
		
		
		<fo:block-container absolute-position="fixed"  top="160.5mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:if test="//payments!=''">
					<xsl:value-of select="format-number(//payments,'#.##0,00', 'el_GR')" />
				</xsl:if>
			</fo:block>
		</fo:block-container>		
		
		<fo:block-container absolute-position="fixed"  top="166mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:if test="//purchases!=''">
					<xsl:value-of select="format-number(//purchases,'#.##0,00', 'el_GR')" />
				</xsl:if>
			</fo:block>
		</fo:block-container>
		
		
		<fo:block-container absolute-position="fixed"  top="172mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:if test="//new_balance!=''">
					<xsl:value-of select="format-number(//new_balance,'#.##0,00', 'el_GR')" />
				</xsl:if>
			</fo:block>
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="178.5mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:if test="//credit_limit!=''">
					<xsl:value-of select="format-number(//credit_limit,'#.##0,00', 'el_GR')" />
				</xsl:if>
			</fo:block>
		</fo:block-container>
		
		<fo:block-container absolute-position="fixed"  top="183.5mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="//interest"/>
			</fo:block>
		</fo:block-container>	
		
		<fo:block-container absolute-position="fixed"  top="188.5mm" right="8mm" text-align="right">
			<fo:block font="10pt Courier New" font-weight="bold"> 
				<xsl:value-of select="//withdr"/>
			</fo:block>
		</fo:block-container>									
		
	</xsl:template>

	<xsl:template name="format-date1">
		<xsl:param name="date" />       
		<xsl:variable name="day" select="substring($date,7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,3,2)" />
		<xsl:value-of select="$day" />/<xsl:value-of select="$month" />/<xsl:value-of select="$year" />           
	</xsl:template>
	
	<xsl:template name="format-date2">
		<xsl:param name="date" />       
		<xsl:variable name="day" select="substring($date,7,2)" />
		<xsl:variable name="month" select="substring($date,5,2)" />
		<xsl:variable name="year" select="substring($date,3,2)" />
		<xsl:value-of select="$day" />-<xsl:value-of select="$month" />-<xsl:value-of select="$year" />           
	</xsl:template>
		
</xsl:stylesheet>
